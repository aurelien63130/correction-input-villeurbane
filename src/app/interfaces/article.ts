export interface Article {
  id: number;
  titre: string;
  description: string;
  image: string;
}
