import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {Article} from "../interfaces/article";


@Component({
  selector: 'app-liste-article',
  templateUrl: './liste-article.component.html',
  styleUrls: ['./liste-article.component.css']
})
export class ListeArticleComponent implements OnInit {

  @Output() voteArticleEmiter = new EventEmitter<string>();

  articles: Article[] = [
    {
      'id':1,
      'titre': 'Le clermont foot en L1',
      'description': 'super saison',
      'image': 'clermont-foot.png'
    },
    {
      'id':2,
      'titre': 'Bel air camp prend feu',
      'description': 'Dommage !!',
      'image': 'belair.png'
    },
    {
      'id':3,
      'titre': 'La FFF dégage Deschamps',
      'description': 'Bon débarras',
      'image': 'deschamps.jpg'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  voteArticle($event: string) {
    this.voteArticleEmiter.emit($event);
  }
}
