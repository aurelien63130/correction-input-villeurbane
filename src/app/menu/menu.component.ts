import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() favoriteArticle = 'Aucun article favori';
  mood : string = '';

  constructor() { }

  ngOnInit(): void {
  }

  showNews(elemToShow: string): void {
    alert(elemToShow);
  }

  changeMood(mood: string){
    this.mood = mood;
  }

}
