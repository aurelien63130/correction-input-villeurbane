import { Component, Input, Output, EventEmitter , OnInit } from '@angular/core';
import {Article} from "../interfaces/article";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() article!: Article;
  @Output() voteArticle = new EventEmitter<string>();
  displayImage = true;

  constructor() { }

  ngOnInit(): void {
    console.log(this.article.description);
  }

  voteForThisArticle() {
    this.voteArticle.emit(this.article.titre);
  }

  toogleImage() {
    this.displayImage = !this.displayImage;
  }


}
