import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'correctionInput';
  favoriteArticle = '';

  voteArticle($event: string) {
    this.favoriteArticle = $event;

    console.log("Mon evenement est maintenant dans le app component");
    console.log(this.favoriteArticle);
    console.log("__end__")
  }
}
